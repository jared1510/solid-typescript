export class GoodParent {
    public findAllGoodPerson(persons: Array<Person>): Array<Person> {
        let results = new Array<Person>();
        for (let person of persons) {
            if ((person.name.startsWith("T") 
                        && ((person.age > 25) 
                        && ((person.hairColor == HairColor.BLACK) 
                        && (person.skinColor == SkinColor.WHITE))))) {
                if ((person.sex == Gender.MALE || person.sex == Gender.FEMALE)) {
                    results.push(person);
                }
                
            }
            
        }
        
        return results;
    }
    
}
class Person {
    
    name: String;
    
    age: number;
    
    sex: Gender;
    
    hairColor: HairColor;
    
    skinColor: SkinColor;
}
enum SkinColor {
    
    BLACK,
    
    BROWN,
    
    WHITE,
}
enum HairColor {
    
    RED,
    
    BLACK,
    
    YELLOW,
}
enum Gender {
    
    FEMALE,
    
    MALE,
    
    GAY,
    
    LES,
}