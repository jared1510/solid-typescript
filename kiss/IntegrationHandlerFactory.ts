import {EmailIntegrationHandler} from "./EmailIntegrationHandler";
import {SMSIntegrationHandler} from "./SMSIntegrationHandler";
import {PushIntegrationHandler} from "./PushIntegrationHandler";
import {IntegrationHandler} from "./IntegrationHandler";

export class IntegrationHandlerFactory {

    private static EMAIL: String = "abc";

    private static SMS: String = "dce";

    private static PUSH: String = "glk";

    private emailHandler: EmailIntegrationHandler;

    private smsHandler: SMSIntegrationHandler;

    private pushHandler: PushIntegrationHandler;

    public constructor (emailHandler: EmailIntegrationHandler, smsHandler: SMSIntegrationHandler, pushHandler: PushIntegrationHandler) {
        this.emailHandler = emailHandler;
        this.smsHandler = smsHandler;
        this.pushHandler = pushHandler;
    }

    public getHandlerFor(integration: String): IntegrationHandler {
        if (IntegrationHandlerFactory.EMAIL === (integration)) return this.emailHandler;
        if (IntegrationHandlerFactory.SMS === (integration))   return this.smsHandler;
        if (IntegrationHandlerFactory.PUSH === (integration)) {
            return this.pushHandler;
        }
        else 
        {
            throw new Error(("No handler found for integration: " + integration));
        }

    }
}
