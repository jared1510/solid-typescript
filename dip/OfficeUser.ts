import {IDatabase} from "../dip/IDatabase";
import {IPost} from "../dip/IPost";

export class OfficeUser {
    private db: IDatabase;
    private post: IPost; 

    constructor(db: IDatabase, post: IPost){
        this.db = db;
        this.post = post;
    }
    public publishNewPost() {
        let postMessage: String = 'example message';
        this.post.createPost(this.db, postMessage);
    }
}
