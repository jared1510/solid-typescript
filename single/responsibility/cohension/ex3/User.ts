import {FileUtil} from "./FileUtil";
import {Database} from "./Database";
import {ErrorDB} from "./ErrorDB";

class User {

    public CreatePost(db: Database, postMessage: String, er: ErrorDB) {
        try {
            db.Add(postMessage);
        }
        catch (ex :Error) {
            er.logError("An error occured: ", ex.toString());
            FileUtil.writeAllText("LocalErrors.txt", ex.toString());
        }

    }
}

