
export class Student {

    id: number;

    name: String;

    age: number;


    public setId(id: number) {
        this.id = id;
    }


    public setName(name: String) {
        this.name = name;
    }

    
    public setAge(age: number) {
        this.age = age;
    }

    
    public getId(): number {
        return this.id;
    }

    public getName(): String {
        return this.name;
    }

    public getAge(): number {
        return this.age;
    }

}

