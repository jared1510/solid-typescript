import { Square } from '../ex1/Square'
class SquareAreaCalculator extends Square {
    public calculateArea(): number {
        var realSquare = this.sizeSquare * this.sizeSquare;
        return realSquare;
    }

    public calculateP(): number {
        var realP = this.sizeSquare * 4;
        return realP;
    }

}


