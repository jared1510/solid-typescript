export class Square {
    
    private _size: number;

    get sizeSquare(){
        return this._size;
    }
    set sizeSquare(size: number){
        this._size = size;
    }

}