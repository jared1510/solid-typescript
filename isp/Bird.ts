import { Flyable } from "./Flyable";
import { Walkable } from "./Walkable";

export class Bird implements Walkable, Flyable {

    public walk() {
        console.log("bird can walk");
    }

    public fly() {
        console.log("bird can fly");
    }
}
