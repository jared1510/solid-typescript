import { Walkable } from "./Walkable";

export class Dog implements Walkable {
    
    public walk() {
        console.log("dog can walk");
    }
    
    public fly() {
        throw new Error();
    }
}
